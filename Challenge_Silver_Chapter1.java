import java.util.Scanner;

public class Challenge_Silver_Chapter1 {
	
	static void menuUtama() {
		System.out.println("----------------------------------------");
		System.out.println("Kalkulator Penghitung Luas dan Volume");
		System.out.println("----------------------------------------");
		System.out.println("Menu");
		System.out.println("1. Hitung Luas Bidang");
		System.out.println("2. Hitung Volume");
		System.out.println("0. Tutup Aplikasi");
		System.out.println("----------------------------------------");
		System.out.print("Ketikkan Angka Pilihan Anda -> ");
		
		
		//Membuat variabel untuk input user
		int masukkan;
		Scanner inputUser = new Scanner(System.in);
		masukkan = inputUser.nextInt();
		
		//Percabangan untuk pemilihan menu
		if (masukkan == 1) {
			menuLuas();
		} else if (masukkan == 2) {
				menuVolume();
			} else if (masukkan == 0) {
				System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
				System.exit(masukkan);
				} else {
					//Ketika user memasukkan angka lain maka akan kembali memanggil method awal
					System.out.println("[ANDA HANYA DAPAT MENGETIKAN ANGKA 1, 2, ATAU 3!]");
					menuUtama();
				}
	}
	
	static void menuLuas() {
		System.out.println("----------------------------------------");
		System.out.println("Pilih Bidang yang akan dihitung");
		System.out.println("----------------------------------------");
		System.out.println("1. Persegi");
		System.out.println("2. Lingkaran");
		System.out.println("3. Segitiga");
		System.out.println("4. Persegi Panjang");
		System.out.println("0. Kembali Ke Menu Sebelumnya");
		System.out.println("----------------------------------------");
		System.out.print("Ketikkan Angka Pilihan Anda -> ");
		
		int masukan;
		Scanner inputUser = new Scanner(System.in);
		masukan = inputUser.nextInt();
		switch (masukan) {
		case 1:
			persegi();
			break;
		case 2:
			lingkaran();
			break;
		case 3:
			segitiga();
			break;
		case 4:
			persegiPanjang();
			break;
		case 0:
			menuUtama();
			break;
			
		default:
			//Ketika user memasukkan angka lain maka akan kembali memanggil method luas untuk memasukkan angka yang benar
			System.out.println("[ANDA HANYA DAPAT MENGETIKAN ANGKA 1, 2, 3, 4 ATAU 0!]");
			menuLuas();
			break;
		}
	}
	
	static void persegi() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih persegi");
		System.out.print("Masukkan sisi: ");
		
		double hasil,masukkan;
		Scanner inputUser = new Scanner(System.in);
		masukkan = inputUser.nextInt();
		hasil = masukkan * masukkan;
		
		System.out.println("Luas Bidang dari  Persegi adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
		
	}
	
	static void persegiPanjang() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Persegi Panjang");
		System.out.print("Masukkan Panjang: ");
		
		double hasil,masukkan1, masukkan2;
		Scanner inputUser1 = new Scanner(System.in);
		masukkan1 = inputUser1.nextInt();
		
		System.out.print("Masukkan Lebar: ");

		Scanner inputUser2 = new Scanner(System.in);
		masukkan2 = inputUser2.nextInt();
		hasil = masukkan2 * masukkan1;
		
		System.out.println("Luas Bidang dari Persegi Panjang adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	static void segitiga() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Segitiga");
		System.out.print("Masukkan Alas: ");
		double hasil,masukkan1, masukkan2;
		Scanner inputUser1 = new Scanner(System.in);
		masukkan1 = inputUser1.nextInt();
		
		System.out.print("Masukkan Tinggi: ");

		Scanner inputUser2 = new Scanner(System.in);
		masukkan2 = inputUser2.nextInt();
		hasil =  0.5 * masukkan2 * masukkan1;
		
		System.out.println("Luas Bidang dari Segitiga adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	static void lingkaran() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Lingkaran");
		System.out.print("Masukkan Jari-Jari: ");
		double hasil,masukkan;
		Scanner inputUser = new Scanner(System.in);
		masukkan = inputUser.nextInt();
		hasil = 3.14 * masukkan * masukkan;
		
		System.out.println("Luas Bidang dari Lingkaran adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	static void menuVolume() {
		System.out.println("----------------------------------------");
		System.out.println("Pilih Bidang yang akan dihitung");
		System.out.println("----------------------------------------");
		System.out.println("1. Kubus");
		System.out.println("2. Balok");
		System.out.println("3. Tabung");
		System.out.println("0. Kembali Ke Menu Sebelumnya");
		System.out.println("----------------------------------------");
		System.out.print("Ketikkan Angka Pilihan Anda -> ");
		
		int masukan;
		Scanner inputUser = new Scanner(System.in);
		masukan = inputUser.nextInt();
		switch (masukan) {
		case 1:
			kubus();
			break;
		case 2:
			balok();
			break;
		case 3:
			tabung();
			break;
		case 0:
			menuUtama();
			break;
			
		default:
			//Ketika user memasukkan angka lain maka akan kembali memanggil method luas untuk memasukkan angka yang benar
			System.out.println("[ANDA HANYA DAPAT MENGETIKAN ANGKA 1, 2, 3, 4 ATAU 0!]");
			menuLuas();
			break;
		}
	}
	
	static void kubus() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Kubus");
		System.out.print("Masukkan Sisi: ");
		double hasil,masukkan;
		Scanner inputUser = new Scanner(System.in);
		masukkan = inputUser.nextInt();
		hasil = Math.pow(masukkan, 3);
		
		System.out.println("Volume Bidang dari Kubus adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	static void balok() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Balok");
		
		System.out.print("Masukkan Panjangr: ");
		double hasil,masukkan1, masukkan2, masukkan3;
		Scanner inputUser1 = new Scanner(System.in);
		masukkan1 = inputUser1.nextInt();
		
		System.out.print("Masukkan Lebar: ");
		Scanner inputUser2 = new Scanner(System.in);
		masukkan2 = inputUser2.nextInt();
		
		System.out.print("Masukkan Tinggi: ");
		Scanner inputUser3 = new Scanner(System.in);
		masukkan3 = inputUser3.nextInt();
		hasil = masukkan3 * masukkan2 * masukkan1;
		
		System.out.println("Volume Bidang dari Balok adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	static void tabung() {
		System.out.println("----------------------------------------");
		System.out.println("Anda memilih Lingkaran");
		System.out.print("Masukkan Tinggi: ");
		double hasil,masukkan1, masukkan2;
		Scanner inputUser1 = new Scanner(System.in);
		masukkan1 = inputUser1.nextInt();
		
		System.out.print("Masukkan Jari-Jari: ");
		Scanner inputUser2 = new Scanner(System.in);
		masukkan2 = inputUser2.nextInt();
		hasil = 3.14 * masukkan1 * masukkan2;
		System.out.println("Volume Bidang dari Tabung adalah " + hasil);
		System.out.println("----------------------------------------");
		System.out.println("Terima Kasih Telah Menggunakan Aplikasi Ini.");
	}
	
	public static void main(String[] args) {
		//Memanggil method menu pertama
		menuUtama();
	}
}
